CORRIDOR
Mester Márk
MT3GVY

A játék lényege, hogy miközben egy folyosón haladunk előre, a szemben jövő falakat kell kikerülnünk, a folyosó elforgatásával. Minél távolabb jutunk annál több pontot kapunk és 500 pontonként a nehézségi szint is növekedik. “Halálunk” után a játék bekéri nevünk 3 karakteres rövidítését és ha eredményünkkel bekerültünk a legjobb 5 közé, akkor megjelenünk Highscore menüpont alatt.
Főmenü:
 
Start:
Új játékot indítása
Highscore:
A legjobb eredmények 
Exit:
Kilépés

A játék: 
 
Használható billentyűk
Balra nyíl: a képernyő balra forgatása
Jobbra nyíl: a képernyő jobbra forgatása
Esc: kilépés a főmenübe
A játék létrehoz egy highscore.txt fájlt, ide mentődnek az elért eredmények, melyből az 5 legjobb kerül megjelenitésre a játék felületén. 
  

Osztályok:

Bindable: Az INotifyPropertyChanged interfész megvalósítására szolgó absztrakt osztály.
OnPropertyChanged(string) : void
PropertyChanged : PropertyChangedEventHandler
Fal: A falak aktuális méretének számolására létrehozott osztály. A falak mozgatásáért és méretének  változtatásáért felelős metódusok megvalósítása ebben az osztályban történik.
	vonalVastagsag: double
	VonalVastagsag: double
	bejovorandom: int
	Fal(int)
	prop: Rect
	Hide(): void
	Move(): void
	Prop: Rect
Falelem: A folyosóelemek aktuális méretének számolására létrehozott osztály. A folyosóelemek mozgatásáért és méretének  változtatásáért felelős metódusok megvalósítása ebben az osztályban történik.
	vonalVastagsag: double
	VonalVastagsag: double
	bejovorandom: int
	FolyosoElem(int)
	prop: Rect
	Hide(): void
	Move(): void
	Prop: Rect	
GameOverUserControl: A játék végén megjelenő képernyő. A UserControl osztály egy leszármazottja, amire az összes képernyő egy ablakban történő megjelenítéshez volt szükség.
	OnPropertyChanged(string): void
	PropertyChanged: PropertyChangedEventHandler
	mainWindow: MainWindow
	score: int
	Score: int
	userName: string
	UserName: string
	GameOverUserControl(MainWindow, int)
	mainWindow_GombLenyomas(object, KeyEventArgs): void
	Button_Click(object, RoutedEventArgs): void
	gameover: Label
	start: Button
	_contentLoaded: bool
	InitializeComponent(): void
	IComponentConnector.Connect(int, object): void
GameUserControl: A játék képernyője. A UserControl osztály egy leszármazottja, amire az összes képernyő egy ablakban történő megjelenítéshez volt szükség. 
	mainWindow : MainWindow
	GameUserControl(MainWindow)
	timer : DispatcherTimer
	LeftKeyPushed : bool
	RightKeyPushed : bool
	folyosoelemek : List<FolyosoElem> 
	falak : List<Fal>
	pozicio : int
	szog : int
	tickcounter : int
	szintnovelo: int
	Ivl: int
	checked : Rect
	checker2 : Rect
	checker3: Rect
	checker4: Rect
	rnd : Random timer_Tick(object, EventArgs): void 
	mainWindow_GombLenyomas(object, KeyEventArgs): void 
	RightRotate(): void
	LeftRotate(): void
	FolyosoElemGenerate(): void
	FalGenerate(): void
	kiertAKepbol(): void
	GameOver(): void
	kulso : Canvas
	canvas: Canvas
	rotateCanvas: RotateTransform
	Pontlabel : Label
	Pontlabel2 : Label
	_contentLoaded : bool
	InitializeComponent() : void 
	IComponentConnedor.Connect(int, object): void
HighscoreUserControl: A legjobb eredmények megjelenítésére szolgáló osztály. A UserControl osztály leszármazottja.
	mainWindow : MainWindow HighscoreUserControl(MainWindow) Button_Click(object, RoutedEventArgs) : void 
	Window_Loaded(object, RoutedEventArgs) : void 
	highscore : Label 
	scoreTB : TextBlock 
	start : Button 
	_contentLoaded : bool 
	InitializeComponent() : void 
	IComponentConnector.Connect(int, object) : void
	MainWindow: A UserControlok megjelenítésére szolgáló window. A Window osztály leszármazottja.
	tartalom : UserControl 
	Tartalom : UserControl 
	GombLenyomas : EventHandler<KeyEventArgs> (object, KeyEventArgs) MainWindow() 
	OnPropertyChanged(string) : void 
	PropertyChanged: PropertyChangedEventHandler(object,PropertyChangedEventArgs)
	Window_KeyDown(object, KeyEventArgs) : void 
	_contentLoaded : bool InitializeComponent() : void IComponentConnector.Connect(int, object) : void
	MenuUserControl: A főmenü megjelenítésére szolgáló osztály. A UserControl osztály leszármazottja.
	mainWindow : MainWindow 
	MenuUserControl(MainWindow) 
	Button_Click(object, RoutedEventArgs) : void 
	Button_Click_l (object, RoutedEventArgs): void 
	Button_Click_2(object, RoutedEventArgs) : void 
	start : Button _contentLoaded : bool 
	InitializeComponent() : void 
	IComponentConnector.Connect(int, object): void