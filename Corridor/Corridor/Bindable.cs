﻿//-----------------------------------------------------------------------
// <copyright file="Bindable.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corridor
{  
    /// <summary>
       /// A Bindable osztály megvalósítása
   /// </summary>
    internal class Bindable : INotifyPropertyChanged
        {
        /// <summary>
            /// A propertyváltozás esetén meghívott esemény
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
            /// Property változása esetén eseményt vált ki
        /// </summary>
        /// <param name="propertyName">A property melyen a változás történt.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
