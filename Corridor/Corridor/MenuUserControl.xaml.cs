﻿//-----------------------------------------------------------------------
// <copyright file="MenuUserControl.xaml.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Corridor
{
    /// <summary>
    /// Interaction logic for MenuUserControl.xaml
    /// </summary>
    public partial class MenuUserControl : UserControl
    {
        /// <summary>
        /// A főablak
        /// </summary>
        private MainWindow mainWindow;

        /// <summary>
        /// Az osztály konstruktora
        /// </summary>
        /// <param name="mainWindow">a főablak </param>
        public MenuUserControl(MainWindow mainWindow)
        {
            this.InitializeComponent();
            this.mainWindow = mainWindow;
        }

        /// <summary>
        /// a játékindításra kattintás esetén meghívott metódus
        /// </summary>
        /// <param name="sender">küldő objetkum</param>
        /// <param name="e">az eventargs</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.Tartalom = new GameUserControl(this.mainWindow);
        }

        /// <summary>
        /// a kilépésre kattintás esetén meghívott metódus
        /// </summary>
        /// <param name="sender">küldő objetkum</param>
        /// <param name="e">az eventargs</param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        /// <summary>
        /// a highscorera kattintás esetén meghívott metódus
        /// </summary>
        /// <param name="sender">küldő objetkum</param>
        /// <param name="e">az eventargs</param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.mainWindow.Tartalom = new HighscoreUserControl(this.mainWindow);
        }
    }
}
