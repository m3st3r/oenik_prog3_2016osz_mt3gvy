﻿//-----------------------------------------------------------------------
// <copyright file="GameOverUserControl.xaml.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Corridor
{
    /// <summary>
    /// A Játék végét jelző UserControl
    /// </summary>
    public partial class GameOverUserControl : UserControl, INotifyPropertyChanged
    {
        /// <summary>
        /// A Főablak
        /// </summary>
        private MainWindow mainWindow;

        /// <summary>
        /// A megszerzett pontszám
        /// </summary>
        private int score;

        /// <summary>
        /// A játék végén bekért felhasználónév
        /// </summary>
        private string userName;

        /// <summary>
        /// A GameOverUserControl konsrtuktora
        /// </summary>
        /// <param name="mainWindow">az ablak</param>
        /// <param name="pontszam">az elért pontszám</param>
        public GameOverUserControl(MainWindow mainWindow, int pontszam)
        {
            this.InitializeComponent();
            this.mainWindow = mainWindow;
            this.DataContext = this;
            this.UserName = string.Empty;
            this.Score = pontszam;
            this.mainWindow.GombLenyomas += this.MainWindow_GombLenyomas;
        }
       
        /// <summary>
        /// A property változás esetén meghívott esemény
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// A pontszám kívülről is elérhető propertyje 
        /// </summary>
        public int Score
        {
            get { return this.score; }
            set { this.score = value; }
        }

        /// <summary>
        /// A felhaszálónév kívülről is elérhető propertyje
        /// </summary>
        public string UserName
        {
            get
            {
                return this.userName;
            }

            set
            {
                this.userName = value;
                this.OnPropertyChanged("UserName");
            }
        }

        /// <summary>
        /// Property változása esetén eseményt vált ki
        /// </summary>
        /// <param name="propertyName">A megváltozott property</param>
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Meghatározott gombok lenyomásakor lefutó metódus
        /// </summary>
        /// <param name="sender">a küldő objektuom</param>
        /// <param name="e">a lenyomott billentyűt tartalmazo EventArgs</param>
        private void MainWindow_GombLenyomas(object sender, KeyEventArgs e)
        {
            int key = KeyInterop.VirtualKeyFromKey(e.Key);
            if (e.Key == Key.Escape)
            {
                this.mainWindow.GombLenyomas -= this.MainWindow_GombLenyomas;
            }

            if (e.Key == Key.Back && this.userName.Length > 0)
            {
                this.UserName = this.UserName.Remove(this.UserName.Length - 1);
                return;
            }

            if (this.UserName.Length < 3 &&
                char.IsLetter((char)key))
            {
                this.UserName += e.Key;
            }

            if (e.Key == Key.Enter && this.UserName.Length == 3)
            {
                string filename = "highscore.txt";
                List<string> scoreList;
                if (File.Exists(filename))
                {
                    scoreList = File.ReadAllLines(filename).ToList();
                }
                else
                {
                    scoreList = new List<string>();
                }

                scoreList.Add(this.UserName + " " + this.score.ToString());
                var sortedScoreList = scoreList.OrderByDescending(ss => int.Parse(ss.Substring(ss.LastIndexOf(" ") + 1)));
                File.WriteAllLines(filename, sortedScoreList.ToArray());

                this.mainWindow.GombLenyomas -= this.MainWindow_GombLenyomas;
                this.mainWindow.Tartalom = new HighscoreUserControl(this.mainWindow);
            }
        }

        /// <summary>
        /// Az egérkattintásra lefutó metódus
        /// </summary>
        /// <param name="sender">a küldő opjektum</param>
        /// <param name="e">a kattintást tartalmazó EventArgs</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.GombLenyomas -= this.MainWindow_GombLenyomas;
            this.mainWindow.Tartalom = new MenuUserControl(this.mainWindow);
        }
    }
}
