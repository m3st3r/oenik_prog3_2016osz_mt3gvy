﻿//-----------------------------------------------------------------------
// <copyright file="Fal.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace Corridor
{
    /// <summary>
    /// A falakat megvalósító osztály
    /// </summary>
    internal class Fal : Bindable
    {
        /// <summary>
        /// A közeledő falak vastagsága
        /// </summary>
        private double vonalVastagsag;

        /// <summary>
        /// A megjelenő falak véletlenszerűsége
        /// </summary>
        private int bejovorandom;

        /// <summary>
        /// A Rect-ek generálására használt példány
        /// </summary>
        private Rect prop;

        /// <summary>
        /// A falak és azok mozgásának megvalósítása
        /// </summary>
        /// <param name="random">Azt határozza meg, hogy "melyik oldalon" jelenjen meg a fal</param>
        public Fal(int random)
        {
            this.bejovorandom = random;
            this.VonalVastagsag = 0.4;
            switch (random)
            {
                case 0: break;
                case 1:
                    this.prop.X = 296;
                    this.prop.Width = 8;
                    this.prop.Height = this.prop.Width / 4;
                    this.prop.Y = this.prop.X + (this.prop.Width * 3);
                    break;
                case 2:
                    this.prop.X = 296;
                    this.prop.Width = 8;
                    this.prop.Height = this.prop.Width / 4;
                    this.prop.Y = this.prop.X;
                    break;
                case 3:
                    this.prop.X = 296;
                    this.prop.Height = 8;
                    this.prop.Width = this.prop.Height / 4;
                    this.prop.Y = this.prop.X;
                    break;
                case 4:
                    this.prop.X = 296;
                    this.prop.Height = 8;
                    this.prop.Width = this.prop.Height / 4;
                    this.prop.X = this.prop.Y + (this.prop.Height * 3);
                    break;
            }
        }

        /// <summary>
        /// Kivülről is elérhető property
        /// </summary>
        public double VonalVastagsag
        {
            get
            {
                return this.vonalVastagsag;
            }

            set
            {
                this.vonalVastagsag = value;
                this.OnPropertyChanged("VonalVastagsag");
            }
        }

        /// <summary>
        /// A Rect propertyk külső elérésére szolgál
        /// </summary>
        public Rect Prop
        {
            get { return this.prop; }
        }

        /// <summary>
        /// A folyosó elemek eltűntetésére szolgáló metódus
        /// </summary>
        public void Hide()
        {
            this.prop.Width = 0;
            this.prop.Height = 0;

            this.OnPropertyChanged("Prop");
        }

        /// <summary>
        /// A Falak mozgatására szolgáló metódus
        /// </summary>
        public void Move()
        {
            switch (this.bejovorandom)
            {
                case 0: break;
                case 1:
                    this.prop.Scale(1.1, 1.1);
                    this.prop.X = 300 - (this.prop.Width / 2);
                    this.prop.Y = this.prop.X + (this.prop.Height * 3);
                    this.VonalVastagsag *= 1.07;
                    break;
                case 2:
                    this.prop.Scale(1.1, 1.1);
                    this.prop.X = 300 - (this.prop.Width / 2);
                    this.prop.Y = this.prop.X;
                    this.VonalVastagsag *= 1.07;
                    break;
                case 3:
                    this.prop.Scale(1.1, 1.1);
                    this.prop.Y = 300 - (this.prop.Height / 2);
                    this.prop.X = this.prop.Y;
                    this.VonalVastagsag *= 1.07;
                    break;
                case 4:
                    this.prop.Scale(1.1, 1.1);
                    this.prop.Y = 300 - (this.prop.Height / 2);
                    this.prop.X = this.prop.Y + (this.prop.Width * 3);
                    this.VonalVastagsag *= 1.07;
                    break;
            }

            this.OnPropertyChanged("Prop");
        }
    }
}
