﻿//-----------------------------------------------------------------------
// <copyright file="FolyosoElem.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Corridor
{
    /// <summary>
    /// A megjelenő FolyosoElemek megvalósítása
    /// </summary>
    internal class FolyosoElem : Bindable
    {
        /// <summary>
        /// A rect propertyjeinek elérésére szolgáló field
        /// </summary>
        private Rect prop;

        /// <summary>
        /// A folyosóelemek vonalvastagsága
        /// </summary>
        private double vonalVastagsag;

        /// <summary>
        /// A folyosoelemek konstruktora, a default értékekkel létrehozásra szolgál
        /// </summary>
        public FolyosoElem()
        {
            this.prop.X = 296;
            this.prop.Y = this.prop.X;
            this.prop.Height = 8;
            this.prop.Width = this.prop.Height;
            this.VonalVastagsag = 0.4;
        }

        /// <summary>
        /// A Rect propertyk külső elérésére szolgál
        /// </summary>
        public Rect Prop
        {
            get { return this.prop; }
        }

        /// <summary>
        /// A vonalvastagság kívülről is elérhető propertyje
        /// </summary>
        public double VonalVastagsag
        {
            get
            {
                return this.vonalVastagsag;
            }

            set
            {
                this.vonalVastagsag = value;
                this.OnPropertyChanged("VonalVastagsag");
            }
        }

        /// <summary>
        /// A folyosó elemek eltűntetésére szolgáló metódus
        /// </summary>
        public void Hide()
        {
            this.prop.Width = 0;
            this.prop.Height = 0;
            this.OnPropertyChanged("Prop");
        }

        /// <summary>
        /// A falak mozgatására szolgáló metódus
        /// </summary>
        public void Move()
        {
            this.VonalVastagsag *= 1.07;
            this.prop.Scale(1.1, 1.1);
            this.prop.X = 300 - (this.prop.Height / 2);
            this.prop.Y = this.prop.X;
            this.OnPropertyChanged("Prop");
        }
    }
}