﻿//-----------------------------------------------------------------------
// <copyright file="HighscoreUserControl.xaml.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Corridor
{
    /// <summary>
    /// A legtöbb pontot elérők listájának megjelenítésére szolgáló osztály
    /// </summary>
    public partial class HighscoreUserControl : UserControl
    {
        /// <summary>
        /// a főablak
        /// </summary>
        private MainWindow mainWindow;
        
        /// <summary>
        /// az osztály konstruktora
        /// </summary>
        /// <param name="mainWindow">a főablak</param>
        public HighscoreUserControl(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.InitializeComponent();
        }

        /// <summary>
        /// A kattintásra lefutó metódus
        /// </summary>
        /// <param name="sender">a küldő objektum</param>
        /// <param name="e">a kiváltott eventargs</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.Tartalom = new MenuUserControl(this.mainWindow);
        }

        /// <summary>
        /// Az ablak betöltéség végző metódus
        /// </summary>
        /// <param name="sender">a küldő objektum</param>
        /// <param name="e">a kiváltott eventargs</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string filename = "highscore.txt";
            int top = 1;
            List<string> scoreList = File.ReadAllLines(filename).ToList();
            foreach (string oneScore in scoreList)
            {
                scoreTB.Text += top.ToString() + ". " + oneScore + "\n";

                top++;
            }
        }
    }
}
