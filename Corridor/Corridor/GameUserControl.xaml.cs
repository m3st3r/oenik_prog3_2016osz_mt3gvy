﻿//-----------------------------------------------------------------------
// <copyright file="GameUserControl.xaml.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Corridor
{
    /// <summary>
    /// Interaction logic for GameUserControl.xaml
    /// </summary>
    public partial class GameUserControl : UserControl
    {
        /// <summary>
        /// A főablak
        /// </summary>
        private MainWindow mainWindow;
        
        /// <summary>
        /// A mozgatáshoz szükséges időzítő timere
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// A bal vagy jobb gombok meg lettek-e nyomva
        /// </summary>
        private bool leftKeyPushed = false, rightKeyPushed = false;

        /// <summary>
        /// A folyosoelemek listája
        /// </summary>
        private List<FolyosoElem> folyosoelemek = new List<FolyosoElem>();

        /// <summary>
        /// A falak listája
        /// </summary>
        private List<Fal> falak = new List<Fal>();

        /// <summary>
        /// Az elfordulást határozza meg
        /// </summary>
        private int pozicio = 1;

        /// <summary>
        /// a forgatás közben az elfordulás mértéke szögfokban
        /// </summary>
        private int szog = 0;

        /// <summary>
        /// Az eddig eltelt tickek számlálója
        /// </summary>
        private int tickcounter = 0;

        /// <summary>
        /// a Falak generálásához használt változó amely a tick-ek növekedésével csökken, így minden szintnél egyre gyorsabban generálódnak új falak.
        /// </summary>
        private int szintnovelo = 42;

        /// <summary>
        /// a szint meghatározására használt változó
        /// </summary>
        private int lvl = 1;

        /// <summary>
        /// Az ezen Rect-el való ütközés jelenti a játék végét
        /// </summary>
        private Rect checker1, checker2, checker3, checker4;

        /// <summary>
        /// Random szám generálásra használt változó
        /// </summary>
        private Random rnd = new Random();

        /// <summary>
        /// GameUserControl konstruktora
        /// </summary>
        /// <param name="mainWindow">a mainwindow példány</param>
        public GameUserControl(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.mainWindow.GombLenyomas += this.MainWindow_GombLenyomas;

            this.InitializeComponent();

            this.checker1 = new Rect { Height = 1, Width = 1, X = 300, Y = 590 };
            this.checker2 = new Rect { Height = 1, Width = 1, X = 590, Y = 300 };
            this.checker3 = new Rect { Height = 1, Width = 1, X = 300, Y = 10 };
            this.checker4 = new Rect { Height = 1, Width = 1, X = 10, Y = 300 };
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();
        }

        /// <summary>
        /// Az új folyosóelemek létrehozására szolgáló metódus
        /// </summary>
        public void FolyosoElemGenerate()
        {
            FolyosoElem folyosoElem = new FolyosoElem();
            this.folyosoelemek.Add(folyosoElem);
            Rectangle folyosoRect = new Rectangle
            {
                Stroke = Brushes.Red,
                StrokeThickness = 1
            };

            Binding folyWidth = new Binding
            {
                Source = folyosoElem,
                Path = new PropertyPath("Prop.Width")
            };

            folyosoRect.SetBinding(Rectangle.WidthProperty, folyWidth);
            Binding folyHeight = new Binding
            {
                Source = folyosoElem,
                Path = new PropertyPath("Prop.Height")
            };
            folyosoRect.SetBinding(Rectangle.HeightProperty, folyHeight);
            Binding folyX = new Binding
            {
                Source = folyosoElem,
                Path = new PropertyPath("Prop.X")
            };
            folyosoRect.SetBinding(Canvas.LeftProperty, folyX);
            Binding folyY = new Binding
            {
                Source = folyosoElem,
                Path = new PropertyPath("Prop.Y")
            };
            folyosoRect.SetBinding(Canvas.TopProperty, folyY);
            Binding folyVonalVastagsag = new Binding
            {
                Source = folyosoElem,
                Path = new PropertyPath("VonalVastagsag")
            };
            folyosoRect.SetBinding(Rectangle.StrokeThicknessProperty, folyVonalVastagsag);

            canvas.Children.Add(folyosoRect);
        }

        /// <summary>
        /// A falak generálására szolgáló metódus
        /// </summary>
        public void FalGenerate()
        {
            Fal fal = new Fal(this.rnd.Next(0, 5));
            this.falak.Add(fal);
            Rectangle falRect = new Rectangle
            {
                Stroke = Brushes.Red,
                StrokeThickness = 1,
                Fill = Brushes.Black
            };
            Binding falWidth = new Binding
            {
                Source = fal,
                Path = new PropertyPath("Prop.Width")
            };
            falRect.SetBinding(Rectangle.WidthProperty, falWidth);
            Binding falHeight = new Binding
            {
                Source = fal,
                Path = new PropertyPath("Prop.Height")
            };
            falRect.SetBinding(Rectangle.HeightProperty, falHeight);
            Binding falX = new Binding
            {
                Source = fal,
                Path = new PropertyPath("Prop.X")
            };
            falRect.SetBinding(Canvas.LeftProperty, falX);
            Binding falY = new Binding
            {
                Source = fal,
                Path = new PropertyPath("Prop.Y")
            };

            falRect.SetBinding(Canvas.TopProperty, falY);

            Binding falVonalVastagsag = new Binding
            {
                Source = fal,
                Path = new PropertyPath("VonalVastagsag")
            };
            falRect.SetBinding(Rectangle.StrokeThicknessProperty, falVonalVastagsag);

            canvas.Children.Add(falRect);
        }

        /// <summary>
        /// Annak megállapítására szolgáló metódus, hogy a folyosóelem még látható e a képen
        /// ha nem akkor a litából eltávolításra kerül
        /// </summary>
        public void KiertAKepbol()
        {
            foreach (FolyosoElem folyosoelem in this.folyosoelemek)
            {
                if (folyosoelem.Prop.Height > 600)
                {
                    folyosoelem.Hide();
                    this.folyosoelemek.Remove(folyosoelem);
                    break;
                }
            }

            foreach (Fal fal in this.falak)
            {
                if (fal.Prop.Height > 600 || fal.Prop.Width > 600)
                {
                    fal.Hide();
                    this.falak.Remove(fal);
                    break;
                }
            }
        }

        /// <summary>
        /// A játék menetéhez használt metódus
        /// </summary>
        /// <param name="sender">a meghívó objektum</param>
        /// <param name="e">az Eventargs</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.tickcounter % this.szintnovelo == 0)
            {
                this.FolyosoElemGenerate();
                this.FalGenerate();
            }

            if (this.tickcounter % 500 == 0 && this.tickcounter != 0 && this.lvl < 9)
            {
                this.szintnovelo -= 4;
                this.lvl++;
            }

            this.timer.Interval = new TimeSpan(0, 0, 0, 0, 20);

            this.KiertAKepbol();

            Pontlabel.Content = "pts: " + this.tickcounter++;
            if (this.lvl < 9)
            {
                Pontlabel2.Content = "lvl: " + this.lvl;
            }

            if (this.lvl == 9)
            {
                Pontlabel2.Content = "lvl: MAX";
            }

            foreach (Fal fal in this.falak)
            {
                if (fal.Prop.IntersectsWith(this.checker1) && !(this.szog > 45 || this.szog < -45))
                {
                    this.GameOver();
                }

                if (fal.Prop.IntersectsWith(this.checker2) && !(this.szog > 135 || this.szog < 45))
                {
                    this.GameOver();
                }

                if (fal.Prop.IntersectsWith(this.checker3) && !(this.szog > 225 || this.szog < 135))
                {
                    this.GameOver();
                }

                if (fal.Prop.IntersectsWith(this.checker4) && !(this.szog > 305 || this.szog < 225))
                {
                    this.GameOver();
                }
            }

            if (this.falak != null)
            {
                foreach (FolyosoElem folyosoelem in this.folyosoelemek)
                {
                    folyosoelem.Move();
                }

                foreach (Fal fal in this.falak)
                {
                    fal.Move();
                }
            }

            if (this.leftKeyPushed)
            {
                this.LeftRotate();
            }
            else if (this.rightKeyPushed)
            {
                this.RightRotate();
            }
        }

        /// <summary>
        /// A gomblenyomásokat figyelő metódus
        /// </summary>
        /// <param name="sender">küldö objektum</param>
        /// <param name="e">a lenyomott gombot tartalmazo EventArgs</param>
        private void MainWindow_GombLenyomas(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.timer.Stop();
                this.mainWindow.GombLenyomas -= this.MainWindow_GombLenyomas;
            }

            if (e.Key == Key.Left && !this.rightKeyPushed)
            {
                this.leftKeyPushed = true;
            }

            if (e.Key == Key.Right && !this.leftKeyPushed)
            {
                this.rightKeyPushed = true;
            }
        }

        /// <summary>
        /// A folyosóelem jobbra fogatására használt metódus
        /// </summary>
        private void RightRotate()
        {
            if (this.pozicio == 1 && this.szog == 90)
            {
                this.rightKeyPushed = false;
                this.pozicio++;
            }
            else if (this.pozicio == 2 && this.szog == 180)
            {
                this.rightKeyPushed = false;
                this.pozicio++;
            }
            else if (this.pozicio == 3 && this.szog == 270)
            {
                this.rightKeyPushed = false;
                this.pozicio++;
            }
            else if (this.pozicio == 4 && this.szog == 360)
            {
                this.rightKeyPushed = false;
                this.pozicio = 1;
                this.szog = 0;
            }

            rotateCanvas.Angle = this.szog;
            if (this.rightKeyPushed)
            {
                this.szog += 5;
            }
        }

        /// <summary>
        /// A folyosóelem balra fogatására használt metódus
        /// </summary>
        private void LeftRotate()
        {
            if (this.pozicio == 1 && this.szog == -90)
            {
                this.leftKeyPushed = false;
                this.pozicio = 4;
                this.szog = 270;
            }
            else if (this.pozicio == 2 && this.szog == 0)
            {
                this.leftKeyPushed = false;
                this.pozicio--;
            }
            else if (this.pozicio == 3 && this.szog == 90)
            {
                this.leftKeyPushed = false;
                this.pozicio--;
            }
            else if (this.pozicio == 4 && this.szog == 180)
            {
                this.leftKeyPushed = false;
                this.pozicio--;
            }

            rotateCanvas.Angle = this.szog;
            if (this.leftKeyPushed)
            {
                this.szog -= 5;
            }
        }

        /// <summary>
        /// a játék végén meghívott metódus. megjeleníti a GameOver képernyőt és leállítja a timert
        /// </summary>
        private void GameOver()
        {
            this.timer.Stop();

            this.mainWindow.GombLenyomas -= this.MainWindow_GombLenyomas;
            this.mainWindow.Tartalom = new GameOverUserControl(this.mainWindow, this.tickcounter);
        }
    }
}
