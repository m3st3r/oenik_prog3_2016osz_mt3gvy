﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="OE-NIK">
//// Mester Márk - MT3GVY - CORRIDOR
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Corridor
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// A Usercontrol tartalma
        /// </summary>
        private UserControl tartalom;

        /// <summary>
        /// A MainWindow konstruktora
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.DataContext = this;
            this.Tartalom = new MenuUserControl(this);
        }

        /// <summary>
        /// A gomblenyomásra kiváltott esemény
        /// </summary>
        public event EventHandler<KeyEventArgs> GombLenyomas;

        /// <summary>
        /// a property megváltozásakor kiváltott esemény
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// a tartalom kivülről is látható propertyje
        /// </summary>
        public UserControl Tartalom
        {
            get
            {
                return this.tartalom;
            }

            set
            {
                this.tartalom = value;
                this.OnPropertyChanged("Tartalom");
            }
        }

        /// <summary>
        /// A property megváltozásakor meghívott metódus
        /// </summary>
        /// <param name="propertyName">a property neve</param>
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Gomblenyomáskor lefutó metódus
        /// </summary>
        /// <param name="sender">küldő objektum</param>
        /// <param name="e">a lenyomott gombot tartalmazo EventArgs</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Tartalom = new MenuUserControl(this);
            }

            if (this.GombLenyomas != null)
            {
                this.GombLenyomas(sender, e);
            }
        }
    }
}
